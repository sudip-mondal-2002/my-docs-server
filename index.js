PORT = process.env.PORT || 8000
const mongoose = require('mongoose');
const Document = require("./docs");
require("dotenv").config()
mongoose.connect(process.env.APPSETTING_MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server, {
    cors: {
        origin: '*',
        methods: ['GET', 'POST']
    }
})

const defaultDocStarting = "";
io.on('connection', socket => {
    socket.on("get-document", async documentId => {
        const document = await findOrCreateDoc(documentId);
        socket.join(documentId);
        socket.emit("load-document", document.data);
        socket.on("send-changes", async (delta,data) => {
            await Document.findByIdAndUpdate(documentId,{data:data})
            socket.broadcast.to(documentId).emit("recieve-changes", delta)
        })
    })
})

const findOrCreateDoc= async (id)=>{
    if (id==null) return;
    const document = await  Document.findById(id);
    if(document) return document;
    return await Document.create({_id:id, data:defaultDocStarting})
}
server.listen(PORT);